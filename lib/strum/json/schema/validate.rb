# frozen_string_literal: true

require "strum/service"
require "json-schema"

module Strum
  module Json
    module Schema
      class Validate
        include Strum::Service

        def call
          array_errors = JSON::Validator.fully_validate(args[:schema], input, errors_as_objects: true)
          array_errors.each { |error| add_error(*parse_json_schema_error(error)) }
          output(input)
        end

        def audit
          add_error(:schema, :not_found) unless args[:schema].is_a?(Hash)
        end

        protected

          def parse_json_schema_error(error)
            id = error[:fragment].sub(/#/, "input")
            keys = id.split("/")
            last_key = keys.map { |key| key =~ /[0-9]+/ ? "[#{key}]" : ".#{key}" }.join[1..]
            value = error[:message][0, error[:message].index(/ in schema/)].sub(error[:fragment], last_key)
            [last_key.to_sym, value]
          end
      end
    end
  end
end
